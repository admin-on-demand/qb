#!/bin/bash

set -euxo pipefail

ENVDIR="../qb_env"

git pull

python3 -m venv .venv
source .venv/bin/activate

python3 -m pip install -r requirements.txt
ansible-galaxy install --force -r requirements.yml

if [ ! -d ${ENVDIR} ]; then
  echo "creating top directory to hold the environments"
  mkdir -v ${ENVDIR}
  pushd ${ENVDIR}

#  git clone git@dev.mirserver.de:noeku/inventory.git
#  mv inventory noeku

#  git clone git@dev.mirserver.de:servers/inventory.git
#  mv inventory mircloud
  popd
fi

for ENV in ${ENVDIR}/*/; do
  pushd ${ENV}
  git checkout master
  git pull
  popd
  if [ -f ${ENV}group_vars/all/ansible_user.yml ]; then
    echo ${ENV}group_vars/all/ansible_user.yml exists
#    ansible-playbook -i ${ENV}/inventory.ini local.yml
  else
    echo ${ENV}group_vars/all/ansible_user.yml missing
    read -p "Enter your ansible_username, usually your last name, lowercase : " inputname
#    ansible-playbook -i ${ENV}/inventory.ini local.yml -u ${inputname}
  fi
done

pushd inventories
./copy_all_2_envs.sh
popd

# source .venv/bin/activate
