location /.well-known/acme-challenge/ {
    alias /nfs/mir/letsencrypt/challenges/;
    try_files $uri =404;
}
