force_color_prompt=yes
color_prompt=yes
full_hostname="$(hostname -f)"

parse_git_branch() {
 git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

if [ "$color_prompt" = yes ]; then
    if [ $(id -u) -eq 0 ]; then # you are root, make the prompt red
        PS1='${debian_chroot:+($debian_chroot)}\[\033[1;31m\]\u\[\033[1;32m\]@\h\[\033[0;32m\].`hostname -d`\[\033[00m\]:\[\033[01;34m\]\w\[\033[01;31m\]$(parse_git_branch)\[\033[00m\]\n>'
    else
        PS1='${debian_chroot:+($debian_chroot)}\[\033[0;32m\]\u@\[\033[1;32m\]\h\[\033[0;32m\].`hostname -d`\[\033[00m\]:\[\033[01;34m\]\w\[\033[01;31m\]$(parse_git_branch)\[\033[00m\]\n>'
    fi
else
 PS1='${debian_chroot:+($debian_chroot)}\u@$full_hostname:\w$(parse_git_branch)\$ '
fi

unset color_prompt force_color_prompt
